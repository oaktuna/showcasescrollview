//
//  TabViewCell.m
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/27/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "TabViewCell.h"

@implementation TabViewCell

- (id)init{
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self addSubview:self.imgView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.imgView setFrame:CGRectMake(4.0, 8.0, 104.0, 50.0)];
}

@end
