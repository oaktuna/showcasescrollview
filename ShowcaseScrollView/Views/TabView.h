//
//  TabView.h
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/27/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "OAFlowView.h"
#import "StyledPageControl.h"

@protocol TabViewDelegate;

@interface TabView : UIView<OAFlowViewDelegate,OAFlowViewDataSource>

@property(nonatomic,strong) OAFlowView *flowView;

@property(nonatomic,assign)id <TabViewDelegate>delegate;

- (void)configure;

@end


@protocol TabViewDelegate <NSObject>

- (void)TabView:(TabView *)tView didSelectAtIndex:(NSUInteger)index UserInfo:(NSDictionary *)info;
- (void)TabView:(TabView *)tView willChangePage:(NSUInteger)index;

@end