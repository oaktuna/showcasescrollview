//
//  TabViewCell.h
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/27/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "OAFlowViewCell.h"

@interface TabViewCell : OAFlowViewCell

@property(nonatomic,strong) UIImageView *imgView;

@end
