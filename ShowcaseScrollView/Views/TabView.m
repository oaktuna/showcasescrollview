//
//  TabView.m
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/27/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "TabView.h"
#import "TabViewCell.h"

@implementation TabView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)configure {
    //Nekadarlik bir genislik icinde scroll edilecegi.
    self.flowView = [[OAFlowView alloc]initWithFrame:CGRectMake(5, 10, [UIScreen mainScreen].bounds.size.width - 15.0, 60.0)];
    [self.flowView setDelegate:self];
    [self.flowView setDataSource:self];
    
    [self addSubview:self.flowView];
}

#pragma mark - OAFlowView Datasources
- (NSUInteger)OAFlowViewNumberOfObjects:(OAFlowView *)flowView{
    return 10;
}

- (OAFlowViewCell *)OAFlowView:(OAFlowView *)flowview CellForIndex:(NSUInteger)index{
    TabViewCell *cell = (TabViewCell *)[flowview dequeueRecycledCell];
    
    if (cell == nil) {
        cell = [[TabViewCell alloc]init];
    }
    
    [cell.imageView setImage:[UIImage imageNamed:@"markafoni.png"]];
    
    return cell;
}

#pragma mark - OAFlowView Delegates
- (void)OAFlowView:(OAFlowView *)flowView willChangePage:(NSUInteger)Index{
    if ([self.delegate respondsToSelector:@selector(TabView:willChangePage:)]) {
        [self.delegate TabView:self willChangePage:Index];
    }
    
}

- (void)OAFlowViewDidSelectCell:(OAFlowViewCell *)flowViewCell AtIndex:(NSUInteger)Index{
    if ([self.delegate respondsToSelector:@selector(TabView:didSelectAtIndex:UserInfo:)]) {
        [self.delegate TabView:self didSelectAtIndex:Index UserInfo:nil];
    }
}

- (CGFloat)OAFlowViewVerticalInset:(OAFlowView *)flowview {
    return 0;
}

- (CGFloat)OAFlowViewHorizontalInset:(OAFlowView *)flowView {
    return 0;
}

- (NSUInteger)OAFlowViewNumberOfColumns:(OAFlowView *)flowView {
    return 3;
}

@end
