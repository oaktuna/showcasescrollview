//
//  OAFlowView.h
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/26/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "OAFlowViewCell.h"

@class OAFlowView;

#pragma mark -
#pragma mark OAFlowViewDataSource

@protocol OAFlowViewDataSource<NSObject>

- (NSUInteger)OAFlowViewNumberOfObjects:(OAFlowView *)flowView;

- (OAFlowViewCell *)OAFlowView:(OAFlowView *)flowView CellForIndex:(NSUInteger)index;

@end

#pragma mark -
#pragma mark OAFlowViewDelegate

@protocol OAFlowViewDelegate<NSObject,UIScrollViewDelegate>

@optional
- (void)OAFlowViewDidSelectCell:(OAFlowViewCell *)flowViewCell AtIndex:(NSUInteger)Index;

- (void)OAFlowView:(OAFlowView *)flowView willChangePage:(NSUInteger)Index;

- (NSUInteger)OAFlowViewNumberOfColumns:(OAFlowView *)flowView;

- (CGFloat)OAFlowViewHorizontalInset:(OAFlowView *)flowView;
- (CGFloat)OAFlowViewVerticalInset:(OAFlowView *)flowView;
- (CGFloat)OAFlowViewPadding:(OAFlowView *)flowView;

@end




@interface OAFlowView : UIScrollView {
    NSMutableSet *visibleCells;
    NSMutableSet *recycledCells;
    
    OAFlowViewCell *touchedFlowViewCell;
    
    //Flags
    BOOL setContentSizeRequired;
    BOOL selectedCell;
}

@property (nonatomic,assign) id <OAFlowViewDataSource> dataSource;
@property (nonatomic,assign) id <OAFlowViewDelegate> delegate;
@property (nonatomic,assign) NSUInteger currentPage;

- (void)initFlowView;

- (void)preparePages;
- (OAFlowViewCell *)dequeueRecycledCell;
- (BOOL)isDisplayingCellForIndex:(NSUInteger)index;
- (void)configureCell:(OAFlowViewCell *)cell forIndex:(NSUInteger)index;

- (void)reloadAllObjects;
- (CGSize)requiredContentSize;
- (NSUInteger)numberOfObjects;
- (NSUInteger)numberOfColumns;
- (void)willChangePage:(NSUInteger)Index;
- (void)setCurrentPage:(NSUInteger)Index animated:(BOOL)animated;

@end
