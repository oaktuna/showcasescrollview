//
//  OAFlowViewCell.h
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/26/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    OAFlowViewCellSelectionStyleGray,
    OAFlowViewCellSelectionStyleBlue,
    OAFlowViewCellSelectionStyleBlack,
    OAFlowViewCellSelectionStyleNone
}OAFlowViewCellSelectionStyle;

@interface OAFlowViewCell : UIView {
    UIView *selectionView;
    OAFlowViewCellSelectionStyle selectionStyle;
}

@property (assign) NSUInteger index;
@property(nonatomic,strong)UIImageView *imageView;

- (void)setSelectionStyle:(OAFlowViewCellSelectionStyle)style;
- (void)setHighlighted:(BOOL)value animated:(BOOL)animated;

@end


@interface UIColor (OAFlowView)

+ (UIColor *) OAFlowViewCellSelectionStyleGray;
+ (UIColor *) OAFlowViewCellSelectionStyleBlue;
+ (UIColor *) OAFlowViewCellSelectionStyleBlack;

@end