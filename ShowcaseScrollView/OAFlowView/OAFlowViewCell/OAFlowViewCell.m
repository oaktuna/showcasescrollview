//
//  OAFlowViewCell.m
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/26/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "OAFlowViewCell.h"

@implementation OAFlowViewCell

- (void)initFlowViewCell{
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [_imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:_imageView];
    
    selectionStyle = OAFlowViewCellSelectionStyleBlue;
    selectionView = [[UIView alloc] initWithFrame:CGRectZero];
    [selectionView setAlpha:0];
    [selectionView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self addSubview:selectionView];
    
    [self setBackgroundColor:[UIColor clearColor]];
}

- (id)init{
    self = [super init];
    if (self) {
        [self initFlowViewCell];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initFlowViewCell];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [_imageView setFrame:self.bounds];
}

- (void)setSelectionStyle:(OAFlowViewCellSelectionStyle)style{
    selectionStyle = style;
    
    switch (style) {
        case OAFlowViewCellSelectionStyleGray:
            [selectionView setBackgroundColor:[UIColor OAFlowViewCellSelectionStyleGray]];
            break;
        case OAFlowViewCellSelectionStyleBlue:
            [selectionView setBackgroundColor:[UIColor OAFlowViewCellSelectionStyleBlue]];
            break;
        case OAFlowViewCellSelectionStyleBlack:
            [selectionView setBackgroundColor:[UIColor OAFlowViewCellSelectionStyleBlack]];
            break;
        case OAFlowViewCellSelectionStyleNone:
            [selectionView setBackgroundColor:[UIColor clearColor]];
            break;
            
        default:
            break;
    }
    
}

- (void)setHighlighted:(BOOL)value animated:(BOOL)animated{
    if (value) {
        [selectionView setAlpha:0.5];
        [selectionView setFrame:self.bounds];
        [self bringSubviewToFront:selectionView];
    }
    else {
        if (!animated) {
            [selectionView setAlpha:0];
            [selectionView setFrame:CGRectZero];
        }
        else {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [selectionView setAlpha:0];
            
            [UIView commitAnimations];
        }
        
    }
}

@end


@implementation UIColor (OAFlowView)

+ (UIColor *) OAFlowViewCellSelectionStyleGray{
    return [UIColor grayColor];
}
+ (UIColor *) OAFlowViewCellSelectionStyleBlue{
    return [UIColor blueColor];
}
+ (UIColor *) OAFlowViewCellSelectionStyleBlack{
    return [UIColor blackColor];
}

@end