//
//  OAFlowView.m
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/26/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "OAFlowView.h"

@implementation OAFlowView


- (void)setDelegate:(id <OAFlowViewDelegate>)theDelegate {
    [super setDelegate:theDelegate];
}
- (id<OAFlowViewDelegate>)delegate {
    return (id<OAFlowViewDelegate>)[super delegate];
}

- (id)init{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self initFlowView];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initFlowView];
    }
    return self;
}

- (void)initFlowView {
    recycledCells = [[NSMutableSet alloc] init];
    visibleCells  = [[NSMutableSet alloc] init];
    setContentSizeRequired = YES;
    selectedCell = NO;
    self.pagingEnabled = YES;
    self.currentPage = 0;
    
    [self setShowsVerticalScrollIndicator:NO];
    [self setShowsHorizontalScrollIndicator:NO];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (setContentSizeRequired) {
        [self setContentSize:[self requiredContentSize]];
        setContentSizeRequired = NO;
    }
    
    CGFloat pageWidth = self.frame.size.width;
    int page = floor((self.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    [self willChangePage:page];
    self.currentPage = page;
    [self preparePages];
}

- (CGSize)requiredContentSize {
    NSUInteger numberOfObjects = [self numberOfObjects];
    NSUInteger numberOfColumns = [self numberOfColumns];
    
    NSUInteger numberOfPages = ceil((double)numberOfObjects / (double)numberOfColumns);
    
    return CGSizeMake((self.frame.size.width * numberOfPages) + 1, self.frame.size.height - 1);
}


- (void)reloadAllObjects {
    [self setContentSize:[self requiredContentSize]];
    // Remove all the cells
    for (OAFlowViewCell *cell in visibleCells) {
        [recycledCells addObject:cell];
        [cell removeFromSuperview];
    }
    
    recycledCells = [[NSMutableSet alloc] init];
    visibleCells  = [[NSMutableSet alloc] init];
    
    // Add all the cells again
    [self preparePages];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self reloadAllObjects];
    [self setCurrentPage:self.currentPage animated:NO];
}

- (void)setCurrentPage:(NSUInteger)Index animated:(BOOL)animated {
    self.currentPage = Index;
    
    [self scrollRectToVisible:CGRectMake(self.currentPage * self.frame.size.width, 0, self.frame.size.width, self.frame.size.height) animated:animated];
}

- (CGSize)sizeForCell {
    return CGSizeMake(self.frame.size.width, self.frame.size.height);
}
- (NSUInteger)numberOfObjects{
    return [[self dataSource] OAFlowViewNumberOfObjects:self];
}

- (void)preparePages {
    
    int firstNeededCellIndex = 0;
    int lastNeededCellIndex  = (int)([self numberOfObjects] - 1);
    
    firstNeededCellIndex = MAX(firstNeededCellIndex, 0);
    lastNeededCellIndex  = (int)(MIN(lastNeededCellIndex, [self numberOfObjects] - 1));
    
    // Recycle no-longer-visible pages
    for (OAFlowViewCell *cell in visibleCells) {
        if (cell.index < firstNeededCellIndex || cell.index > lastNeededCellIndex) {
            [recycledCells addObject:cell];
            [cell removeFromSuperview];
        }
    }
    
    //minusSet param === recycledCells, The set of objects to remove from the receiving set.
    [visibleCells minusSet:recycledCells];
    
    // add missing pages
    for (int index = firstNeededCellIndex; index <= lastNeededCellIndex; index++) {
        if (![self isDisplayingCellForIndex:index]) {
            OAFlowViewCell *cell = [[self dataSource] OAFlowView:self CellForIndex:index];
            [self configureCell:cell forIndex:index];
            [self addSubview:cell];
            [visibleCells addObject:cell];
        }
    }
}

- (OAFlowViewCell *)dequeueRecycledCell {
    OAFlowViewCell *cell = [recycledCells anyObject];
    if (cell) {
        [recycledCells removeObject:cell];
    }
    return cell;
}

- (BOOL)isDisplayingCellForIndex:(NSUInteger)index {
    BOOL foundPage = NO;
    for (OAFlowViewCell *cell in visibleCells) {
        if (cell.index == index) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}

- (void)configureCell:(OAFlowViewCell *)cell forIndex:(NSUInteger)index{
    
    cell.index = index;
    
    CGFloat buttonWidth = (self.frame.size.width - ([self verticalInset] * 2) - ([self padding] * ([self numberOfColumns] - 1))) / [self numberOfColumns];
    NSUInteger numberOfColumns = [self numberOfColumns];
    
    NSUInteger insetNumber = ((floor((double) index / (double) numberOfColumns) * 2) + 1);
    CGFloat inset =  insetNumber * [self verticalInset];
    
    CGFloat padding = ([self padding] * index) - (((insetNumber - 1) / 2) * [self padding]);
    
    CGFloat x =  inset + (index * buttonWidth) + padding;
    CGFloat y = [self horizontalInset];
    CGFloat width = buttonWidth;
    CGFloat height = self.frame.size.height - ([self horizontalInset] * 2);
    
    [cell setFrame:CGRectMake(x, y, width , height)];
    [cell layoutSubviews];
}

#pragma mark -
#pragma mark Delegate Methods

- (void)willChangePage:(NSUInteger)Index {
    if ([[self delegate] respondsToSelector:@selector(OAFlowView:willChangePage:)]) {
        [[self delegate] OAFlowView:self willChangePage:Index];
    }
}

- (void)didSelectCell:(OAFlowViewCell *)flowViewCell AtIndex:(NSUInteger)Index {
    if ([[self delegate] respondsToSelector:@selector(OAFlowViewDidSelectCell:AtIndex:)]) {
        [[self delegate] OAFlowViewDidSelectCell:flowViewCell AtIndex:Index];
    }
}

- (NSUInteger)numberOfColumns {
    NSUInteger numberOfColumns;
    if ([[self delegate] respondsToSelector:@selector(OAFlowViewNumberOfColumns:)]) {
        numberOfColumns = [[self delegate] OAFlowViewNumberOfColumns:self];
    }
    else {
        numberOfColumns = 1;
    }
    return numberOfColumns;
}

- (CGFloat)horizontalInset {
    CGFloat horizontalInset;
    
    if ([[self delegate] respondsToSelector:@selector(OAFlowViewHorizontalInset:)]) {
        horizontalInset = [[self delegate] OAFlowViewHorizontalInset:self];
    }
    else {
        horizontalInset = 10;
    }
    
    return horizontalInset;
}

- (CGFloat)verticalInset {
    CGFloat verticalInset;
    
    if ([[self delegate] respondsToSelector:@selector(OAFlowViewVerticalInset:)]) {
        verticalInset = [[self delegate] OAFlowViewVerticalInset:self];
    }
    else {
        verticalInset = 10;
    }
    return verticalInset;
}

- (CGFloat)padding {
    CGFloat padding;
    
    if ([[self delegate] respondsToSelector:@selector(OAFlowViewPadding:)]) {
        padding = [[self delegate] OAFlowViewPadding:self];
    }
    else {
        padding = 10;
    }
    return padding;
}


#pragma mark -
#pragma mark Touch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    
    if (!selectedCell) {
        if ([[self hitTest:location withEvent:event] isKindOfClass:[OAFlowViewCell class]]) {
            touchedFlowViewCell = (OAFlowViewCell *)[self hitTest:location withEvent:event];
            [touchedFlowViewCell setHighlighted:YES animated:NO];
            [touchedFlowViewCell setSelectionStyle:OAFlowViewCellSelectionStyleBlue];
            selectedCell = YES;
        }	
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [touchedFlowViewCell setHighlighted:NO animated:NO];
    selectedCell = NO;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [touchedFlowViewCell setHighlighted:NO animated:YES];
    selectedCell = NO;
    
    [self didSelectCell:touchedFlowViewCell AtIndex:[touchedFlowViewCell index]];
}

@end
