//
//  ShowCaseViewController.m
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/26/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "ShowCaseViewController.h"

@interface ShowCaseViewController ()

@end

@implementation ShowCaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabView = [[TabView alloc]initWithFrame:CGRectMake(0.0, 150.0, [UIScreen mainScreen].bounds.size.width, 60.0)];
    [self.tabView setDelegate:self];
    [self.tabView configure];
    
    self.pageControl = [[StyledPageControl alloc]initWithFrame:CGRectMake((([UIScreen mainScreen].bounds.size.width - 105.0) / 2.0), 220.0, 105.0, 25.0)];
    [self.pageControl setStrokeWidth:0];
    [self.pageControl setGapWidth:4];
    [self.pageControl setDiameter:15];
    
    [self.pageControl setCurrentPage:0];
    [self.pageControl setPageControlStyle:PageControlStyleDefault];
    [self.pageControl setCoreNormalColor:[UIColor colorWithRed:255.0 / 255.0
                                                         green:255.0 / 255.0 blue:255.0 / 255.0 alpha:1]];
    [self.pageControl setCoreSelectedColor:[UIColor colorWithRed:189.0 / 255.0
                                                           green:195.0 / 255.0 blue:199.0 / 255.0 alpha:1]];
    self.pageControl.alpha = 1.0f;
    
    [self.pageControl setNumberOfPages:4];
    
    if ([self.pageControl numberOfPages] < 2) {
        self.pageControl.hidden = YES;
    }
    
    [self.view addSubview:self.pageControl];

    
    [self.view addSubview:self.tabView];
}

#pragma mark - TabView Delegate
- (void)TabView:(TabView *)tView willChangePage:(NSUInteger)index {
    [self.pageControl setCurrentPage:index];
}

- (void)TabView:(TabView *)tView didSelectAtIndex:(NSUInteger)index UserInfo:(NSDictionary *)info {
    NSLog(@"tab view selected index %ld",(long)index);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
