//
//  ShowCaseViewController.h
//  ShowcaseScrollView
//
//  Created by Omer Aktuna on 2/26/15.
//  Copyright (c) 2015 aktuna. All rights reserved.
//

#import "TabView.h"

@interface ShowCaseViewController : UIViewController<TabViewDelegate>

@property(nonatomic,strong) TabView *tabView;
@property(nonatomic,strong) StyledPageControl *pageControl;

@end
